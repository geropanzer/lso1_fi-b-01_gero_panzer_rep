import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class vertwisten {

	public static void main(String[] args) {
		//Einlesen eines Wortes
		String zutwistendesWort = einlesen();
		//Wort twisten
		char [] erg = twister(zutwistendesWort);
		System.out.print("Das Wort getwistet lautet: ");
        System.out.println(erg);
		
		
        //Wörterliste in ArrayList übertragen 
        ArrayList<String> array = new ArrayList<String>();
        String meinString;
	    try {
		BufferedReader in = new BufferedReader (
                     new FileReader ("woerterliste.txt") );
		try {
		    while( (meinString = in.readLine()) != null ) {
			array.add(meinString);
		    }
		    in.close();
		} catch (IOException e) {
		    System.out.println("Read error " + e);
		}
	     } 
	     catch (IOException e) {
	     System.out.println("Open error " + e);
	     }
	    //Check ob Wort in Wörterliste [Arraylist] enthalten ist
	    //System.out.println(array.contains(back));
	    
        //Getwistet Wort auf String zum vergleichen mit Wörterliste überschreiben
	    String back;
	    back = String.copyValueOf(erg);
	    int counter =0;
	    long t= System.currentTimeMillis();
	    long end = t+30000;
        do{
            back = backtwist(back);
            counter +=1;
        }while(!(array.contains(back)) && System.currentTimeMillis() < end);
        if (array.contains(back)) {
        System.out.println("Nach " +counter+" Versuchen wurde das Wort: "+back+" in der Wörterliste gefunden.");
        }else {
        	System.out.println("Nach 30 Sekunden wurde kein Treffer in der Wörterliste gefunden. NOTAUS!");
        }
        
}
	public static String einlesen () {
		// Einlesen eines Wortes
		Scanner tastatur = new Scanner(System.in);
        System.out.print("Geben Sie ein Wort ein:");
        String zutwistendesWort = tastatur.next();
        tastatur.close();
        
        return zutwistendesWort;
	}
	public static char[] twister (String zutwistendesWort) {
		//Twisten des eingelesenen Wortes
		char[] ha = new char[zutwistendesWort.length()];
        char[] ch = new char[zutwistendesWort.length()];
        
    	for(int j=0;j <zutwistendesWort.length();j++){
    		ch[j] = '_';
    		ha[j] = zutwistendesWort.charAt(j);
    	}
        for(int i=0;i<zutwistendesWort.length()-1;i++) {
        	ch[0] = zutwistendesWort.charAt(0);
        	ch[zutwistendesWort.length()-1]=zutwistendesWort.charAt(zutwistendesWort.length()-1);
        	
        	Random rand = new Random();
        	int integer =rand.nextInt(zutwistendesWort.length()-2)+1;
        	while (ha[integer]=='-') {
        		integer =rand.nextInt(zutwistendesWort.length()-2)+1;
        	}
        	if (ch[i]=='_') {
        		ch[i] = ha[integer];
        		ha[integer] ='-';	
        	} 	
        }
        //System.out.println(zutwistendesWort);
        return ch;
	}
	public static String backtwist(String back) {
		//Zurücktwisten des Wortes
		back = String.copyValueOf(twister(back));
		
		return back;
	}
}
