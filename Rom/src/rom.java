import java.util.Scanner;

public class rom {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie eine r�mische Zahl an (max. 6999): ");
		String eingabe = tastatur.next();
		tastatur.close();
		String eingabeKontrolle = eingabe;
		System.out.println(eingabeKontrolle);
		String vergleich = "";
		int erg = 0;
		String[] romArray = { "III", "XXX", "CCC", "MMM", "II", "IV", "IX", "XX", "XL", "XC", "CC", "CD", "CM", "MM","I", "V", "X", "L", "C", "D", "M" };
		int[] deziArray = { 3, 30, 300, 3000, 2, 4, 9, 20, 40, 90, 200, 400, 900, 2000, 1, 5, 10, 50, 100, 500, 1000 };
		for (int i = 0; i < romArray.length; i++) {
			if (eingabe.contains(romArray[i])) {
				erg += deziArray[i];
				eingabe = eingabe.replaceFirst(romArray[i], "");
			}
		}
		int ergebnis = erg;
		if (eingabe.isEmpty()) {
		    while (erg !=0) {
			if (erg >=1000) {
				erg -= 1000;
				vergleich = vergleich + "M";
			}  else if (erg >=900) {
				erg -= 900;
				vergleich = vergleich + "CM";
			} else if (erg >=500) {
				erg -= 500;
				vergleich = vergleich + "D";
			} else if (erg >=400) {
				erg -= 400;
				vergleich = vergleich + "CD";
			} else if (erg >=100) {
				erg -= 100;
				vergleich = vergleich + "C";
			} else if (erg >=90) {
				erg -= 90;
				vergleich = vergleich + "XC";
			} else if (erg >=50) {
				erg -= 50;
				vergleich = vergleich + "L";
			} else if (erg >=40) {
				erg -= 40;
				vergleich = vergleich + "XL";
			} else if (erg >=10) {
				erg -= 10;
				vergleich = vergleich + "X";
			}else if (erg >=9) {
				erg -= 9;
				vergleich = vergleich + "IX";
			} else if (erg >=5) {
				erg -= 5;
				vergleich = vergleich + "V";
			} else if (erg >=4) {
				erg -= 4;
				vergleich = vergleich + "IV";
			} else if (erg >=1) {
				erg -= 1;
				vergleich = vergleich + "I";
			}
		    }
			if (vergleich.equals(eingabeKontrolle.trim())) {
				System.out.println("Diese Zahl entspricht im dezimalen System: " + ergebnis);
			}else System.out.println("Falsche Eingabe!");
			
		} else {
			System.out.println("Falsche Eingabe!");
		}
	}
}
