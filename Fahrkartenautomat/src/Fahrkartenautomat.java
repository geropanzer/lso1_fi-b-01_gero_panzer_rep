﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      //Im Moodle gibt es keine Abgabefläche für das PAP-Diagramm
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       int weitereBestellung = 1;
       while(weitereBestellung == 1) {
       //Eingabe
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       System.out.println("Wollen Sie weitere Tickets ordern? ");
       Scanner tastatur = new Scanner(System.in);
       System.out.println("Ja [1] Nein[2]");
       weitereBestellung= tastatur.nextInt();
       if(weitereBestellung !=1) {
    	   System.out.println("Bis zur nächsten Bestellung!");
       }
       }

    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        System.out.println("Welche Fahrkarte brauchen Sie? ");
        System.out.print("Einzelticket  2,90€ [0]");
        System.out.print("Doppelticket  5,80€ [1]");
        System.out.print("Tagesticket   6,50€ [2]");
        System.out.print("Gruppenticket 9,90€ [3]");
        int ticketNummer = tastatur.nextInt();
        double zuzahlenderBetrag;
        switch(ticketNummer) {
        case 0: 
        	System.out.println("Sie haben sie für das Einzelticket entschieden.");
        	zuzahlenderBetrag =2.90;
        	break;
        case 1: 
        	System.out.println("Sie haben sie für das Doppelticket entschieden.");
        	zuzahlenderBetrag =5.80;
        	break;
        case 2: 
        	System.out.println("Sie haben sie für das Tagesticket entschieden.");
        	zuzahlenderBetrag =6.50;
        	break;
        case 3: 
        	System.out.println("Sie haben sie für das Gruppenticket entschieden.");
        	zuzahlenderBetrag =9.90;
        	break;
        default: 
        	System.out.println("Ungültige Eingabe. Es wird das Einzelticket ausgewählt(2,90€).");
        	zuzahlenderBetrag =2.90;	
        }
        System.out.println("Anzahl der Tickets:");
        int anzahlTickets = tastatur.nextInt();;
        while(!(anzahlTickets < 10 && anzahlTickets >=1)) {
        	anzahlTickets = tastatur.nextInt();
        	System.out.println("Ungültige Anzahl an Tickets, geben sie eine neue Anzahl ein: ");
        }
        return anzahlTickets * zuzahlenderBetrag;
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        int zeit = 1000;
        warte(zeit);
        System.out.println("\n\n");  	
    }
    public static void rückgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag +0.00005;
        if(rückgabebetrag > 0.00)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n" ,rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
            {
              muenzeAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
              muenzeAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzeAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
              muenzeAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
              muenzeAusgeben(5, "CENT");
  	          rückgabebetrag -= 0.05;
  	
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    }
    public static void warte(int millisekunde) {
//      for (int i = 0; i < 8; i++)                // Variante mit "="-Zwischenzeile
//      {
//         System.out.print("=");
//         try {
//			Thread.sleep(250);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//      }

    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.printf("\n%2d " + einheit,betrag);
    }
}